import numpy as np
import typing


class Schedule(object):

    def __init__(self, n_weekdays: int, n_slots_day: int, n_rooms: int,
                 n_tracks: int, courses: typing.Dict[str, typing.List[int]]):
        """
        Data structure containing all information and functions required for
        building a schedule

        :param n_weekdays: the number of days in the week that this schedule can
        span
        :param n_slots_day: the number of time-slots in a day on which courses
        can be scheduled
        :param n_rooms: The number of rooms that this schedule can span
        :param n_tracks: The number of tracks that are being offered in the
        schedule
        :param courses: A dict of the courses. The key is of type string (the
        name), and the value is a list containing integers, each integer
        representing a track that has to follow this course. For each course,
        it is guaranteed that it belongs to at least one track.
        """
        self.n_weekdays = n_weekdays
        self.n_slots_day = n_slots_day
        self.n_rooms = n_rooms
        self.n_tracks = n_tracks
        self.courses = courses
        self.schedule = np.empty(shape=(n_weekdays, n_slots_day, n_rooms), dtype=str)

    """
       ____                                  _   _               _     
      / __ \                                | | | |             | |    
     | |  | |_      ___ __    _ __ ___   ___| |_| |__   ___   __| |___ 
     | |  | \ \ /\ / / '_ \  | '_ ` _ \ / _ \ __| '_ \ / _ \ / _` / __|
     | |__| |\ V  V /| | | | | | | | | |  __/ |_| | | | (_) | (_| \__ \
      \____/  \_/\_/ |_| |_| |_| |_| |_|\___|\__|_| |_|\___/ \__,_|___/
    
    
    """
    def get_tracks_for_course(self, course) -> typing.List[int]:
        """
        Get the list of tracks connected to a certain course
        :param course: Course letter
        :return: Empty list when not found, else list of tracks following this course
        """
        if course == '' or course is None:
            return []
        elif course in self.courses:
            return self.courses[course]
        return []


    def count_courses_in_schedule(self) -> int:
        """
        Counts tota courses in the schedule
        :return: int
        """
        count = 0
        for i in range (self.n_weekdays):
            for j in range (self.n_slots_day):
                for h in range (self.n_rooms):
                    if self.schedule[i, j, h] != '':
                        count += 1
        return count

    def get_courses_for_track(self, track):
        """
        Get the list of courses connected to a certain track
        :param track: track
        :return: Empty list if track is not valid, else list of courses connected to this track
        """
        courses = []
        if track < 0 or track is None:
            return []
        else:
            for course in self.courses:
                if track in self.courses[course]:
                    courses.append(course)
        return courses


    """
       _____               _          _                  _   _               _     
      / ____|             | |        | |                | | | |             | |    
     | |  __ _ __ __ _  __| | ___  __| |  _ __ ___   ___| |_| |__   ___   __| |___ 
     | | |_ | '__/ _` |/ _` |/ _ \/ _` | | '_ ` _ \ / _ \ __| '_ \ / _ \ / _` / __|
     | |__| | | | (_| | (_| |  __/ (_| | | | | | | |  __/ |_| | | | (_) | (_| \__ \
      \_____|_|  \__,_|\__,_|\___|\__,_| |_| |_| |_|\___|\__|_| |_|\___/ \__,_|___/
                                                                                   
                                                                                   
    """
    def print_schedule(self, track: typing.Optional[int]) -> None:
        """
        Prints a schedule, in free format.

        :param track: if track is given (not None), the schedule will only
        contain courses belonging to a given track
        """
        # Bool to check if print is for only track, or all
        only_display_one = track is not None
        string = ''
        # Loop over all days
        for i in range(self.n_weekdays):
            # Horizontal print so multiple loops
            string += f'Day {i}\n'
            for j in range(self.n_slots_day):
                string += f'     Slot {j}      '
            string += '\n'
            string += '+--------------+ ' * self.n_slots_day + '\n'
            for k in range(self.n_rooms):
                for l in range(self.n_slots_day):
                    # Als het maar voor 1 track moet geprint worden
                    if only_display_one:
                        string += f"|   Room {k}: {self.schedule[i, l, k] if self.schedule[i, l, k] != '' and track in self.get_tracks_for_course(self.schedule[i,l,k]) else ' '}  | "

                    else:
                        string += f"|   Room {k}: {self.schedule[i,l,k] if self.schedule[i,l,k] != ''else ' '}  | "
                string += '\n'
            string += '+--------------+ ' * self.n_slots_day + '\n\n'
        # Print the final string at once
        print(string)

    def count_courses_on_day(self, day: int, track: int) -> int:
        """
        Counts the number of courses a given track has on a given day

        :param day: the day to check
        :param track: the track to check
        :return: The number of courses belonging to that track
        """
        courses = self.get_courses_for_track(track)
        count = 0
        for x in self.schedule[day]:
            for room in x:
                if room in courses:
                    count += 1
        return count


    def duplicate_track_courses_on_slot(self, day: int, slot: int,
                                        track: int) -> bool:
        """
        Checks whether for a given slot on a given day, a duplicate entrees exist for a given track.

        :param day: the day to check
        :param slot: the slot to check
        :param track: the track to check

        :return: True if there are duplicates, False otherwise
        """
        # self.print_schedule(None)
        courses = self.get_courses_for_track(track)
        count = 0
        for course in self.schedule[day, slot]:
            if course in courses:
                count += 1
        return count > 1


    def count_intervals_on_day(self, day: int, track: int) -> int:
        """
        Counts the number of interval slot for a track on a given day. A slot s
        is considered an interval slot if there exists a pair of slots (b,e)
        that also offer a course in that track, such that b < s < e.

        :param day: The day to check
        :param track: The track to check
        :return: True if the track complies with the interval requirement
        """
        # self.print_schedule(None)
        slots = self.schedule[day]
        courses = self.get_courses_for_track(track)
        start, end = 0, 0
        intervals = 0

        # Get start and end of school hours for this track
        for i in range(len(slots)):
            slot = slots[i]
            for room in slot:
                if room in courses and i > start == 0:
                    start = i
                elif room in courses and i > end:
                    end = i
        # Get the total count of open positions
        for slot in slots[start:end + 1]:
            open_positions = 0
            for room in slot:
                if room == '' or room not in courses:
                    open_positions += 1
            if open_positions == len(slot):
                intervals += 1
        return intervals


    def is_safe_schedule(self, lastOne = False) -> bool:
        """
        This method determines if the schedule as defined in this class is still valid
        according to the rules stated in the assignment
        :return: True or False
        """
        if self.count_courses_in_schedule() > 1:
            for i in range (self.n_weekdays):
                for j in range (self.n_tracks):
                    n_courses = self.count_courses_on_day(i,j)
                    n_intervals = self.count_intervals_on_day(i,j)
                    n_courses_for_track = len(self.get_courses_for_track(j))
                    for k in range(self.n_slots_day):
                        duplicates = self.duplicate_track_courses_on_slot(i, k, j)
                        if (n_intervals > 1 and n_intervals > 0) or duplicates or (lastOne and (n_courses > 0 and n_courses < 2) and n_courses_for_track > 1):
                            return False
        return True

    def _build_schedule_recursive(
            self, start_day: int, start_slot: int, start_room: int,
            courses: typing.Dict[str, typing.List[int]]) -> np.array:
        """
        Private function. Feel free to determine your own set of arguments.
        Will not be called from unit tests, but only by this function and
        build_schedule_backtracking

        Function builds the schedule in a recursive way, using backtracking.
        """
        # Apparently there are not enough spots for this schedule
        if len(courses) > (self.n_weekdays * self.n_slots_day * self.n_rooms):
            return None
        elif not len(courses):
            if not self.is_safe_schedule(True):
                return None
            return self.schedule

        if start_room >= self.n_rooms:
            start_room = 0
            return self._build_schedule_recursive(start_day, start_slot + 1, start_room, courses)
        elif start_slot >= self.n_slots_day:
            start_room, start_slot = 0, 0
            return self._build_schedule_recursive(start_day + 1, start_slot, start_room, courses)
        elif start_day >= self.n_weekdays:
            return None

        for k, v in courses.items():
            if self.is_safe_schedule():
                self.schedule[start_day, start_slot, start_room] = k
                courses_2 = courses.copy()
                used_course = courses_2.pop(k)
                if start_slot + 1 == self.n_slots_day and start_room + 1 == self.n_rooms:
                    good_tracks = 0
                    for i in range(self.n_tracks):
                        course_count = self.count_courses_on_day(start_day, i)
                        if (course_count >= 2 or course_count == 0) or len(self.get_courses_for_track(i)) < 2:
                            good_tracks += 1
                    if good_tracks == self.n_tracks and self.is_safe_schedule():
                        if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_2) is not None:
                            return self.schedule
                        else:
                            courses_2[k] = v
                            self.schedule[start_day, start_slot, start_room] = ''
                            if self._build_schedule_recursive(start_day, start_slot, start_room+1, courses_2) is not None:
                                return self.schedule

                elif self.is_safe_schedule():
                    if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_2) is not None:
                        return self.schedule
                    else:
                        courses_2[k] = v
                        self.schedule[start_day, start_slot, start_room] = ''
                        if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_2) is not None:
                            return self.schedule
                self.schedule[start_day, start_slot, start_room] = ''

        # if self.is_safe_schedule():
        #     courses_2 = courses.copy()
        #     if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_2) is not None:
        #         return self.schedule
        return None

    def build_schedule_backtracking(self) -> np.array:
        """
        Function that builds a schedule using backtracking. Make sure to make
        use of the functions that check the validity of the schedule throughout.
        If you encounter a schedule that you already know to be invalid, you
        should backtrack and try another option.
        This function can be implemented by a single call to another function,
        _build_schedule_recursive. You are allowed to change the argumentes of
        the function _build_schedule_recursive

        :return: a valid schedule is such exists, None otherwise
        """
        self.print_schedule(None)
        print(self.courses)
        schedule = self._build_schedule_recursive(0, 0, 0, self.courses)
        self.print_schedule(None)
        return schedule

    def build_schedule_greedy(self) -> np.array:
        """
        Function that iteratively builds a schedule in a greedy way. Think of
        smart constructions to build a schedule that respects the aforementioned
        rules as much as possible.

        :return: a schedule that attempts to respect the previous rules as much
        as possible.
        """
        self.print_schedule(None)
        day, slot, room = 0, 0, 0
        courses = self.courses.copy()
        print(courses)

        # put all courses down with no real broken rules
        #
        for course, tracks in courses.items():
            self.schedule[day, slot, room] = course
            incorrect_tracks = 0

            for track in range(self.n_tracks):
                n_courses = self.count_courses_on_day(day, track)
                n_intervals = self.count_intervals_on_day(day, track)
                has_duplicate = self.duplicate_track_courses_on_slot(day, slot, track)

                if n_intervals > 1 or has_duplicate:
                    incorrect_tracks += 1

            if incorrect_tracks > 0:
                self.schedule[day, slot, room] = ''
            room += 1

            # Check if it's in the range, otherwise change
            #
            if room >= self.n_rooms:
                room = 0
                slot += 1
            if slot >= self.n_slots_day:
                room, slot = 0, 0
                day += 1
            if day >= self.n_weekdays:
                break
        # Place all the courses that were problematic
        # Choose the best option
        #
        for course, tracks in courses.items():
            if course not in self.schedule:
                # How many rules this course is breaking
                errors = 1000
                best_day, best_slot, best_room = 0, 0, 0
                for day in range(self.n_weekdays):
                    for slot in range(self.n_slots_day):
                        for room in range(self.n_rooms):
                            if self.schedule[day, slot, room] == '':
                                self.schedule[day, slot, room] = course
                                incorrect_tracks = 0

                                for track in range(self.n_tracks):
                                    n_courses = self.count_courses_on_day(day, track)
                                    n_intervals = self.count_intervals_on_day(day, track)
                                    has_duplicate = self.duplicate_track_courses_on_slot(day, slot, track)

                                    if n_intervals > 1 or has_duplicate:
                                        incorrect_tracks += 1

                                if incorrect_tracks < errors:
                                    errors = incorrect_tracks
                                    best_day, best_slot, best_room = day, slot, room
                                self.schedule[day, slot, room] = ''
                # Put course on best slot
                self.schedule[best_day, best_slot, best_room] = course
        self.print_schedule(None)
        return self.schedule


