import numpy as np
import typing
import unittest

from schedule import Schedule

class PrivateScheduleFactory(object):

    @staticmethod
    def private_get_schedule_1day() -> Schedule:
        courses = {
            'A': [0, 1],
            'B': [0],
            'C': [1, 3],
            'D': [3],

        }
        schedule = Schedule(n_weekdays=1, n_slots_day=3, n_tracks=4,
                            n_rooms=2, courses=courses)
        return schedule

    @staticmethod
    def private_get_schedule_2days() -> Schedule:
        courses = {
            'A': [0, 1],
            'D': [3],
            'C': [2],
            'E': [3]
        }
        schedule = Schedule(n_weekdays=2, n_slots_day=2, n_tracks=4,
                            n_rooms=4, courses=courses)
        return schedule


    @staticmethod
    def private_get_schedule_1day_long() -> Schedule:
        courses = {
            'A': [0, 1],
            'B': [0],
            'C': [1, 3],
            'D': [3],
            'E': [0, 1],
            'F': [0],
            'G': [1, 3],
            'H': [3],
            'I': [0, 1],
            'J': [0],
            'K': [1, 3],
            'L': [3],
        }

        schedule = Schedule(n_weekdays=1, n_slots_day=16, n_tracks=4,
                            n_rooms=2, courses=courses)
        return schedule

    @staticmethod
    def private_get_schedule_impossible() -> Schedule:
        courses = {
            'A': [0, 1],
            'B': [0],
            'C': [1, 3],
            'D': [3],
        }
        schedule = Schedule(n_weekdays=1, n_slots_day=1, n_tracks=4,
                            n_rooms=2, courses=courses)
        return schedule

    @staticmethod
    def get_schedule_four_tracks_2courses() -> Schedule:
        courses = {
            'A': [0, 1, 2],
            'B': [0, 1],
            'C': [1, 2, 3],
            'D': [3],
            'E': [2, 3],
            'F': [0],
        }
        schedule = Schedule(n_weekdays=2, n_slots_day=3, n_tracks=4,
                            n_rooms=2, courses=courses)
        return schedule

    @staticmethod
    def get_schedule_for_analysis_optimal_method() -> Schedule:
        courses = {
            'A': [0, 1, 2],
            'B': [0, 1],
            'C': [1, 2, 3],
            'D': [3],
            'E': [2, 3],
            'F': [0],
            'G': [0, 1, 2],
            # 'H': [0, 1],
            # 'I': [1, 2, 3],
            # 'J': [3],

        }
        schedule = Schedule(n_weekdays=3, n_slots_day=3, n_tracks=4,
                            n_rooms=2, courses=courses)
        return schedule


class PrivateTestSchedule(unittest.TestCase):

    def _check_solution(self, schedule: Schedule, result: np.array):
        self.assertIsNotNone(result)
        result_flat = result.flatten()
        result_flat = np.delete(result_flat, np.where(result_flat == ''))
        # no duplicates
        self.assertEqual(len(np.unique(result_flat)), len(result_flat))
        # all courses
        self.assertSetEqual(set(result_flat), set(schedule.courses.keys()))

        for day in range(schedule.n_weekdays):
            for track in range(schedule.n_tracks):
                if len(schedule.get_courses_for_track(track)) > 1:
                    self.assertNotEqual(schedule.count_courses_on_day(day, track), 1)
                self.assertLessEqual(schedule.count_intervals_on_day(day, track), 1)

                for slot in range(schedule.n_slots_day):
                    self.assertFalse(schedule.duplicate_track_courses_on_slot(day, slot, track))

    def test_build_schedule(self):
        schedule = PrivateScheduleFactory.private_get_schedule_1day()
        result = schedule.build_schedule_backtracking()
        self._check_solution(schedule, result)

    def test_build_schedule_impossible(self):
        schedule = PrivateScheduleFactory.private_get_schedule_impossible()
        result = schedule.build_schedule_backtracking()
        self.assertIsNone(result)

    def test_build_schedule_long_day(self):
        schedule = PrivateScheduleFactory.private_get_schedule_1day_long()
        result = schedule.build_schedule_backtracking()
        self._check_solution(schedule, result)

    def test_build_schedule_two_days(self):
        schedule = PrivateScheduleFactory.private_get_schedule_2days()
        result = schedule.build_schedule_backtracking()
        self._check_solution(schedule, result)

    def test_greedy_builder(self):
        schedule = PrivateScheduleFactory.get_schedule_four_tracks_2courses()
        result = schedule.build_schedule_greedy()
        print(result)
