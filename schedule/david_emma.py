import numpy as np
import typing


class Schedule(object):

    def __init__(self, n_weekdays: int, n_slots_day: int, n_rooms: int,
                 n_tracks: int, courses: typing.Dict[str, typing.List[int]]):
        """
        Data structure containing all information and functions required for
        building a schedule

        :param n_weekdays: the number of days in the week that this schedule can
        span
        :param n_slots_day: the number of time-slots in a day on which courses
        can be scheduled
        :param n_rooms: The number of rooms that this schedule can span
        :param n_tracks: The number of tracks that are being offered in the
        schedule
        :param courses: A dict of the courses. The key is of type string (the
        name), and the value is a list containing integers, each integer
        representing a track that has to follow this course. For each course,
        it is guaranteed that it belongs to at least one track.
        """
        self.n_weekdays = n_weekdays
        self.n_slots_day = n_slots_day
        self.n_rooms = n_rooms
        self.n_tracks = n_tracks
        self.courses = courses
        self.schedule = np.empty(shape=(n_weekdays, n_slots_day, n_rooms), dtype=str)

    def print_schedule(self, track: typing.Optional[int]) -> None:
        """
        Prints a schedule, in free format

        :param track: if track is given (not None), the schedule will only
        contain courses belonging to a given track
        """

        for day in range(self.n_weekdays):
            for time_slots in range(self.n_slots_day):
                for rooms in range(self.n_rooms):
                    current_course = self.schedule[day, time_slots, rooms]
                    if current_course != '':
                        if track in self.courses[current_course]:
                            print("time slot: " + str(time_slots) + " rooms: " + str(
                                rooms) + " " + "course: " + current_course + " " + "for track: " + str(track))
                        if track is None:
                            print("time slot: " + str(time_slots) + " rooms: " + str(
                                rooms) + " " + "course: " + current_course)

    def count_courses_on_day(self, day: int, track: int) -> int:
        """
        Counts the number of courses a given track has on a given day

        :param day: the day to check
        :param track: the track to check
        :return: The number of courses belonging to that track
        """

        teller = 0
        for time_slots in range(self.n_slots_day):
            for rooms in range(self.n_rooms):
                current_course = self.schedule[day, time_slots, rooms]
                if current_course != '' and track in self.courses[current_course]:
                    teller += 1

        return teller

    def duplicate_track_courses_on_slot(self, day: int, slot: int,
                                        track: int) -> bool:
        """
        Checks whether for a given slot on a given day, a duplicate entrees exist for a given track.

        :param day: the day to check
        :param slot: the slot to check
        :param track: the track to check

        :return: True if there are duplicates, False otherwise
        """
        teller = 0
        duplicate = False
        for rooms in range(self.n_rooms):
            current_course = self.schedule[day, slot, rooms]
            if current_course != '' and track in self.courses[current_course]:
                teller += 1

        if teller >= 2:
            duplicate = True

        return duplicate

    def count_intervals_on_day(self, day: int, track: int) -> int:
        """
        Counts the number of interval slots for a track on a given day. A slot s
        is considered an interval slot if there exists a pair of slots (b,e)
        that also offer a course in that track, such that b < s < e.

        :param day: The day to check
        :param track: The track to check
        :return: True if the track complies with the interval requirement
        """

        start_course, end_course = 0, 0
        interval_count = 0

        for slot in range(self.n_slots_day):
            schedule_item = self.schedule[day, slot]
            for room in self.schedule[day, slot]:
                if room in self.courses.keys():
                    if track in self.courses[room]:
                        if slot > start_course == 0:
                            start_course = slot
                        elif slot > end_course:
                            end_course = slot

        for pos in self.schedule[day][start_course:end_course + 1]:
            open_for_slot = 0
            for r in pos:
                if r == '' or track not in self.courses[r]:
                    open_for_slot += 1
            if open_for_slot == len(pos):
                interval_count += 1
        return interval_count




    def _build_schedule_recursive(
            self, start_day: int, start_slot: int, start_room: int,
            courses: typing.Dict[str, typing.List[int]]) -> np.array:
        """
        Private function. Feel free to determine your own set of arguments.
        Will not be called from unit tests, but only by this function and
        build_schedule_backtracking
        TODO: don't forget to update the documentation
        """
        total_spots = self.n_weekdays * self.n_slots_day * self.n_rooms
        # Too many courses for the amount of spots
        if len(courses) > total_spots:
            return None
        # No courses at all
        elif not len(courses):
            return self.schedule

        if start_room >= self.n_rooms:
            return self._build_schedule_recursive(start_day, start_slot + 1, 0, courses)

        elif start_slot >= self.n_slots_day:
            return self._build_schedule_recursive(start_day + 1, 0, 0, courses)
        elif start_day >= self.n_weekdays:
            return self.schedule

        for course, tracks in courses.items():

            if self.check_schedule():
                self.schedule[start_day][start_slot][start_room] = course
                courses_copy = courses.copy()
                courses_copy.pop(course)

                if start_slot == self.n_slots_day - 1 and start_room == self.n_rooms - 1:
                    correct_t = 0
                    for track in range(self.n_tracks):
                        count = self.count_courses_on_day(start_day, track)
                        if count >= 2 or count == 0:
                            correct_t += 1
                    if correct_t == self.n_tracks:
                        if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_copy) is not None:
                            return self.schedule
                else:
                    if self._build_schedule_recursive(start_day, start_slot, start_room + 1, courses_copy) is not None:
                        return self.schedule

            self.schedule[start_day][start_slot][start_room] = ''

        return None

    def check_schedule(self):
        number_of_courses = 0
        for i in range(self.n_weekdays):
            for j in range(self.n_tracks):
                number_of_courses += self.count_courses_on_day(i, j)

        if number_of_courses > 1:
            for day in range(self.n_weekdays):
                for track in range(self.n_tracks):
                    courses = self.count_courses_on_day(day, track)
                    intervals = self.count_intervals_on_day(day, track)

                    for slot in range(self.n_slots_day):
                        has_duplicate = self.duplicate_track_courses_on_slot(day, slot, track)
                        if (intervals > 0 and intervals > 1) or has_duplicate:
                            return False
        return True

    def build_schedule_backtracking(self) -> np.array:
        """
        Function that builds a schedule using backtracking. Make sure to make
        use of the functions that check the validity of the schedule throughout.
        If you encounter a schedule that you already know to be invalid, you
        should backtrack and try another option.
        This function can be implemented by a single call to another function,
        _build_schedule_recursive. You are allowed to change the argumentes of
        the function _build_schedule_recursive

        :return: a valid schedule is such exists, None otherwise
        """
        return self._build_schedule_recursive(0, 0, 0, self.courses)


    def build_schedule_greedy(self) -> np.array:
        """
        Function that iteratively builds a schedule in a greedy way. Think of
        smart constructions to build a schedule that respects the aforementioned
        rules as much as possible.

        :return: a schedule that attempts to respect the previous rules as much
        as possible.
        """
        raise NotImplementedError()
